import { CustomersService } from './../customers.service';
import { AuthService } from './../auth.service';
import { PredictService } from './../predict.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Customer } from '../interfaces/customer';
import { Capital } from '../interfaces/capital';
import { Country } from '../interfaces/country';
import { HousesService } from '../houses.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {

  region;
  name;
  flag;
  capital;
  currencies;
  userId;
  id;
  languages;
  timezones;
  customers$;
  population;
  country;
  customers;
  action;
  message="Details succesfully saved";
  spinner:boolean = false;
  seeAll:boolean = false;
  seeCity:boolean = false;
  display;
  countries$:Observable<Country>
  capitales$:Observable<Capital>

  getCountries(){
    console.log(this.region);
    this.seeAll= true;
    this.seeCity = false;
    this.countries$ = this.housesService.getRegion(this.region);
  }

  save(){
    console.log(this.userId);
    console.log(this.id);

    this.customersService.updateCapital(this.userId, this.id, this.country,this.currencies,this.population,this.languages);
    this.router.navigate(['/customers']);

  }

  openSnackBar() {
    this._snackBar.open(this.message,this.action,{
      duration: 2000,
    });
  }
  seeCapital(capital){
    console.log(capital);
    this.seeCity = true;
    this.seeAll= false;
    this.capitales$ = this.housesService.getCapital(capital);
    this.capitales$.subscribe(
      data => {
        this.country = data[0].name;
        this.flag=data[0].flag;
        this.timezones = data[0].timezones;
        this.capital = data[0].capital;
        this.population = data[0].population;
        console.log(data[0].languages[0].name);
        this.currencies = data[0].currencies[0].code;
        this.languages =  data[0].languages[0].name;
        
        
       

       } )
        
      }
  
  constructor(private http: HttpClient,private _snackBar:MatSnackBar, private router:Router, private housesService:HousesService, private authService:AuthService, private customersService:CustomersService) { }

 

  


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
                   console.log(customer);
              }                        
            }
          )
      })
  }   
}