import { HttpClient } from '@angular/common/http';
import {  Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Country } from './interfaces/country';
import { Capital } from './interfaces/capital';






@Injectable({
  providedIn: 'root'
})
export class HousesService {
  
  // private URL = "https://restcountries.eu/rest/v2/a";
  private URL ="https://restcountries.eu/rest/v2/region/";
  private url = "https://restcountries.eu/rest/v2/capital/";

  private bbb = "https://restcountries.eu/rest/v2/name/";

  getCountries(country):Observable<Country>{
    return this.http.get<Country>(`${this.bbb}${country}`); 

  }

  getRegion(region):Observable<Country>{
    return this.http.get<Country>(`${this.URL}${region}`); 
  }

  getCapital(capital):Observable<Capital>{
    return this.http.get<Capital>(`${this.url}${capital}`); 
  }
  

  
  constructor(private http:HttpClient, public router:Router) { }
  
}